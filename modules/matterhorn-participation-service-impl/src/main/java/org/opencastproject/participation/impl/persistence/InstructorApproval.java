/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.opencastproject.participation.model.HasInstructorApproval;
import org.opencastproject.participation.model.Instructor;

/**
 * @author John Crossman
 */
public class InstructorApproval implements HasInstructorApproval {

  private Instructor instructor;
  private boolean approved;

  @Override
  public Instructor getInstructor() {
    return instructor;
  }

  public void setInstructor(final Instructor instructor) {
    this.instructor = instructor;
  }

  @Override
  public boolean isApproved() {
    return approved;
  }

  @Override
  public void setApproved(final boolean approved) {
    this.approved = approved;
  }
}