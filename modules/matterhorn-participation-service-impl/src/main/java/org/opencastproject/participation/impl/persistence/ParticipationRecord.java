/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import java.sql.Timestamp;

/**
 * Entity object for storing participation entities in persistence storage.
 * 
 */
@XmlRootElement

public class ParticipationRecord {

  public ParticipationRecord() {
  }

  public ParticipationRecord(String courseOfferingId, String instructorLdapId, Boolean approved, String captureDevices,
                             String license, String distributionRespositories, Timestamp created) {
    this.courseOfferingId = courseOfferingId;
    this.instructorLdapId = instructorLdapId;
    this.approved = approved;
    this.captureDevices = captureDevices;
    this.license = license;
    this.distributionRepositories = distributionRespositories;
    this.created = created;
  }

  protected Long participationId;

  public Long getParticipationId() {
    return participationId;
  }

  public void setParticipationId(Long participationId) {
    this.participationId = participationId;
  }

  // comma delimited list
  private String captureDevices;

  private String instructorLdapId;
  
  private String courseOfferingId;

  private Boolean approved;

  private String license;

  // comma delimited list
  private String distributionRepositories;

  private Timestamp created;
  
  public String getCourseOfferingId() {
    return courseOfferingId;
  }

  public void setCourseOfferingId(String courseOfferingId) {
    this.courseOfferingId = courseOfferingId;
  }

  public String getDistributionRepositories() {
    return distributionRepositories;
  }

  public void setDistributionRepositories(String distributionRepositories) {
    this.distributionRepositories = distributionRepositories;
  }
  @XmlTransient  // because this will blow up JSON serialization without special handling
  public Timestamp getCreated() {
    return created;
  }

  public void setCreated(Timestamp created) {
    this.created = created;
  }

  public String getCaptureDevices() {
    return captureDevices;
  }

  public void setCaptureDevices(String captureDevices) {
    this.captureDevices = captureDevices;
  }

  @XmlSchemaType(name="string")  
  //because JAXB will "helpfully" return a long without this
  // unfortunately doesn't seem to work w. jax.rs
  public String getInstructorLdapId() {
    return instructorLdapId;
  }

  public void setInstructorLdapId(String instructorLdapId) {
    this.instructorLdapId = instructorLdapId;
  }

  public Boolean getApproved() {
    return approved;
  }

  public void setApproved(Boolean approved) {
    this.approved = approved;
  }

  public String getLicense() {
    return license;
  }

  public void setLicense(String license) {
    this.license = license;
  }

}
