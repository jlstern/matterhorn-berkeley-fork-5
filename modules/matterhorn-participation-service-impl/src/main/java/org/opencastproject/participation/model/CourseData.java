/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author John Crossman
 */
public class CourseData {

  private CanonicalCourse canonicalCourse;
  private Term term;
  private String section;

  private List<DayOfWeek> meetingDays;
  private String startTime;
  private String endTime;

  private String courseOfferingId;
  private Room room;
  private Integer studentCount;

  @XmlElement(name = "participations")
  private Set<Participation> participationSet;

  public Set<Participation> getParticipationSet() {
    return participationSet;
  }

  public void setParticipationSet(final Set<Participation> participationSet) {
    this.participationSet = participationSet;
  }

  public String getCourseOfferingId() {
    return courseOfferingId;
  }

  public void setCourseOfferingId(final String courseOfferingId) {
    this.courseOfferingId = courseOfferingId;
  }

  public CanonicalCourse getCanonicalCourse() {
    return canonicalCourse;
  }

  public void setCanonicalCourse(final CanonicalCourse canonicalCourse) {
    this.canonicalCourse = canonicalCourse;
  }

  public Term getTerm() {
    return term;
  }

  public void setTerm(final Term term) {
    this.term = term;
  }

  public String getSection() {
    return section;
  }

  public void setSection(final String section) {
    this.section = section;
  }

  public List<DayOfWeek> getMeetingDays() {
    return meetingDays;
  }

  public void setMeetingDays(final List<DayOfWeek> meetingDays) {
    this.meetingDays = meetingDays;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(final String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(final String endTime) {
    this.endTime = endTime;
  }

  public Room getRoom() {
    return room;
  }

  public void setRoom(Room room) {
    this.room = room;
  }

  public Integer getStudentCount() {
    return studentCount;
  }

  public void setStudentCount(final Integer studentCount) {
    this.studentCount = studentCount;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("canonicalCourse", canonicalCourse)
        .append("courseOfferingId", courseOfferingId)
        .append("section", section)
        .append("meetingDays", meetingDays)
        .append("startTime", startTime)
        .append("endTime", endTime)
        .append("term", term)
        .append("room", room)
        .append("studentCount", studentCount)
        .toString();
  }

  @Override
  public boolean equals(final Object o) {
    final boolean equals;
    if (o instanceof CourseData) {
      final CourseData that = (CourseData) o;
      equals = new EqualsBuilder()
          .append(this.getCanonicalCourse(), that.getCanonicalCourse())
          .append(this.getTerm(), that.getTerm())
          .isEquals();
    } else {
      equals = false;
    }
    return equals;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(canonicalCourse.getCcn()).append(term).toHashCode();
  }
}
