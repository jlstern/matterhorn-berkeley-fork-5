/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.opencastproject.participation.model.DayOfWeek;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author John Crossman
 */
public final class MVUtils {

  private static final Logger logger = LoggerFactory.getLogger(MVUtils.class);

  /**
   * Columns names of the Oracle materialized view we're using to pull course data. Package-local
   * for sake of unit test.
   */
  public enum StrCol implements MVColumn<String> {
    dept_description, course_title, catalog_id, meeting_start_time, meeting_start_time_ampm_flag, meeting_end_time,
    meeting_end_time_ampm_flag, term_name, term_yr, building_name, meeting_days, section_num, room_number, first_name,
    last_name, email_address, ldap_uid,;

    @Override
    public Class<String> getType() {
      return String.class;
    }
  }

  /**
   * @see StrCol
   */
  public enum IntCol implements MVColumn<Integer> {
    course_cntl_num, student_count;
    @Override
    public Class<Integer> getType() {
      return Integer.class;
    }
  }

  /**
   * @see StrCol
   */
  public enum DateCol implements MVColumn<Date> {
    term_start_date, term_end_date,;
    @Override
    public Class<Date> getType() {
      return Date.class;
    }
  }

  /**
   * Private.
   */
  private MVUtils() {
  }

  /**
   * Examples:
   *  " M  W  " returns list: Monday, Wednesday
   *  "  T T " returns list: Tuesday, Thursday
   * @param untrimmed expected value from materialized view
   * @return according to rules above
   */
  public static List<DayOfWeek> getMeetingDays(final String untrimmed) {
    final String parseThis = StringUtils.stripEnd(untrimmed, null);
    final List<DayOfWeek> meetingDays = new LinkedList<DayOfWeek>();
    final int length = StringUtils.length(parseThis);
    if (length > 0) {
      if (!StringUtils.containsIgnoreCase(parseThis, "unsched")) {
        for (int index = 0; index < length; index++) {
          final char c = parseThis.charAt(index);
          if (c != ' ') {
            final DayOfWeek dayOfWeek = getByIndex(index);
            if (dayOfWeek == null) {
              logger.warn("Null " + DayOfWeek.class.getSimpleName() + " returned for index = " + index);
            } else if (Character.toLowerCase(dayOfWeek.name().charAt(0)) != Character.toLowerCase(c)) {
              throw new IllegalArgumentException(dayOfWeek + " does NOT have the expected leading character: " + c);
            } else {
              meetingDays.add(dayOfWeek);
            }
          }
        }
      }
    }
    return meetingDays.isEmpty() ? null : meetingDays;
  }

  /**
   * @param r one row of results from database query
   * @param column column we are looking for
   * @param <T> data type of column
   * @return value, possibly null
   * @throws SQLException
   */
  public static <T> T get(final ResultSet r, final MVColumn<T> column) throws SQLException {
    try {
      final T result;
      if (String.class.equals(column.getType())) {
        result = (T) r.getString(column.name());
      } else if (Integer.class.equals(column.getType())) {
        result = (T) new Integer(r.getInt(column.name()));
      } else if (Date.class.equals(column.getType())) {
        result = (T) r.getDate(column.name());
      } else {
        throw new UnsupportedOperationException("Unexpected column type: " + column.getType());
      }
      return result;
    } catch (final SQLException e) {
      logger.error("FAILURE on column '" + column.name() + "' where type = " + column.getType().getSimpleName(), e);
      throw e;
    }
  }

  private static DayOfWeek getByIndex(final int index) {
    DayOfWeek dayOfWeek = null;
    final int length = DayOfWeek.values().length;
    if (index >= 0 && index < length) {
      for (final DayOfWeek next : DayOfWeek.values()) {
        if (next.getIndexAccordingToMaterializedView() == index) {
          dayOfWeek = next;
          break;
        }
      }
    } else {
      logger.warn("Index value is expected to be greater than 0 and less than " + length);
    }
    return dayOfWeek;
  }

  private interface MVColumn<T> {
    Class<T> getType();
    String name();
  }
}
