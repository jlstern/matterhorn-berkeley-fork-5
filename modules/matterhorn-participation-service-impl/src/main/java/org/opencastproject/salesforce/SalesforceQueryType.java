/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

/**
 * @author John Crossman
 */
public enum SalesforceQueryType {

  selectAllCourseOfferings("SELECT Id, Name, Section__c, StageName, Approval_Status__c, CCN__c, CourseOfferingID__c, Students_Enrolled__c, "
      + "Schedule_Days__c, End_Time__c, Start_Time__c, Recording_Type__c, Recording_Availability__c, Publish_Delay_Days__c, "
      + "Instructor_1__c, Instructor_1_Approval__c, Instructor_1__r.FirstName,  Instructor_1__r.LastName, Instructor_1__r.Email, Instructor_1__r.Calnet_UID__c, "
      + "Instructor_2__c, Instructor_2_Approval__c, Instructor_2__r.FirstName,  Instructor_2__r.LastName, Instructor_2__r.Email, Instructor_2__r.Calnet_UID__c, "
      + "Instructor_3__c, Instructor_3_Approval__c, Instructor_3__r.FirstName,  Instructor_3__r.LastName, Instructor_3__r.Email, Instructor_3__r.Calnet_UID__c, "
      + "Instructor_4__c, Instructor_4_Approval__c, Instructor_4__r.FirstName,  Instructor_4__r.LastName, Instructor_4__r.Email, Instructor_4__r.Calnet_UID__c, "
      + "Instructor_5__c, Instructor_5_Approval__c, Instructor_5__r.FirstName,  Instructor_5__r.LastName, Instructor_5__r.Email, Instructor_5__r.Calnet_UID__c, "
      + "Instructor_6__c, Instructor_6_Approval__c, Instructor_6__r.FirstName,  Instructor_6__r.LastName, Instructor_6__r.Email, Instructor_6__r.Calnet_UID__c, "
      + "Room__c, Room__r.Building__c, Room__r.Room_Number_Text__c, Room__r.Recording_Capabilities__c, Recordings_Scheduled__c, admin_schedule_override__c,"
      + "Parent_Project__c, Parent_Project__r.Semester_Term__c, Parent_Project__r.Semester_Year__c, Parent_Project__r.Semester_End_Date__c, Parent_Project__r.Semester_Start_Date__c "
      + "FROM Opportunity WHERE Type='Course Capture'"),

  selectCourseOffering(selectAllCourseOfferings.soql + " AND CourseOfferingID__c = '{courseOfferingId}'"),

  selectInstructorByCalNetUID("SELECT Id, LastName, FirstName, Department, Calnet_UID__c, Role__c, Email FROM Contact WHERE Calnet_UID__c = '{calNetUID}'"),

  selectAllRooms("SELECT Id, Building__c, Name, Room_Number_Text__c, Recording_Capabilities__c FROM Locations__c"),

  selectCourseApprovalStatus("SELECT Approval_Status__c FROM Opportunity WHERE Type='Course Capture' AND CourseOfferingID__c = '{courseOfferingId}'"),

  selectCapturePreferences("SELECT Id, Recording_Type__c, Recording_Availability__c, Publish_Delay_Days__c, {instructorFieldName} FROM Opportunity WHERE CourseOfferingID__c = '{courseOfferingId}'"),

  selectCapturePreferencesByAdmin("SELECT Id, Recording_Type__c, Recording_Availability__c, Publish_Delay_Days__c, admin_schedule_override__c FROM Opportunity WHERE CourseOfferingID__c = '{courseOfferingId}'"),

  selectInstructorCapturePreferences("SELECT Id, {instructorFieldName} FROM Opportunity WHERE CourseOfferingID__c = '{courseOfferingId}'"),

  selectRecordingsScheduledTrue("SELECT Id, Recordings_Scheduled__c FROM Opportunity WHERE CourseOfferingID__c = '{courseOfferingId}'"),

  selectCourseOfferingsBySemesterYear("SELECT Id, Name, Section__c, CCN__c, Students_Enrolled__c, Schedule_Days__c, End_Time__c, Start_Time__c, "
      + "Instructor_1__c, Instructor_1_Approval__c, Instructor_2__c, Instructor_2_Approval__c, Instructor_3__c, Instructor_3_Approval__c,"
      + "Instructor_4__c, Instructor_4_Approval__c, Instructor_5__c, Instructor_5_Approval__c, Instructor_6__c, Instructor_6_Approval__c, Room__c, Parent_Project__c "
      + "FROM Opportunity WHERE Type='Course Capture' AND Parent_Project__r.Semester_Term__c = '{semesterTerm}' AND Parent_Project__r.Semester_Year__c = '{semesterYear}'"),

  selectAllInstructors("SELECT Id, LastName, FirstName, Department, Calnet_UID__c, Role__c, Email FROM Contact"),

  selectAllTerms("SELECT Id, Semester_Term__c, Semester_Year__c, Semester_End_Date__c, Semester_Start_Date__c FROM Opportunity "
      + " WHERE Semester_Term__c != null AND Semester_Year__c  != null AND Semester_Start_Date__c != null AND Semester_End_Date__c != null AND isclosed = false");

  private final String soql;

  private SalesforceQueryType(final String soql) {
    this.soql = soql;
  }

  public String getSOQL() {
    return soql;
  }

}
