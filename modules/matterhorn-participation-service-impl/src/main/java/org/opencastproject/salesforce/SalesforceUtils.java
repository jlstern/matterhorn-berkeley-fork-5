/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import com.sforce.ws.ConnectorConfig;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.StringUtil;
import org.opencastproject.util.date.DateUtil;
import org.opencastproject.util.date.TimeOfDay;
import org.opencastproject.util.env.Environment;
import org.opencastproject.util.env.EnvironmentUtil;

import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.bind.XmlObject;

import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author John Crossman
 */
public final class SalesforceUtils {

  /**
   * Private.
   */
  private SalesforceUtils() {
  }

  /**
   * Package-only util for parsing results from Salesforce.
   * @param xmlObject never null
   * @return property representing the name of the node
   */
  static String getLocalPart(final XmlObject xmlObject) {
    return (xmlObject.getName() == null) ? null : xmlObject.getName().getLocalPart();
  }

  /**
   * @return conventional format of strings which represent dates.
   */
  public static SimpleDateFormat getDateFormatConvention() {
    return new SimpleDateFormat("yyyy-MM-dd");
  }

  public static HasSalesforceFieldName getInstructorApprovalFieldName(final CourseOffering courseOffering, final String calNetUID) throws NotFoundException {
    SalesforceFieldInstructor fieldName = null;
    for (final Participation participation : courseOffering.getParticipationSet()) {
      if (calNetUID.equals(participation.getInstructor().getCalNetUID())) {
        fieldName = participation.getSalesforceField();
        break;
      }
    }
    if (fieldName == null) {
      throw new IllegalStateException("User is not authorized to update capture preferences of course offering: " + courseOffering);
    }
    return SalesforceUtils.getInstructorApprovalFieldName(fieldName);
  }

  /**
   * @see org.opencastproject.participation.api.CourseManagementService#updateCapturePreferences(String, String, org.opencastproject.participation.model.CapturePreferences)
   * @param field Null not allowed
   * @return our way of dynamically creating a {@link HasSalesforceFieldName}
   */
  public static HasSalesforceFieldName getInstructorApprovalFieldName(final SalesforceFieldInstructor field) {
    return new HasSalesforceFieldName() {
      @Override
      public String name() {
        return field.name() + "_Approval__c";
      }
    };
  }

  /**
   * @param next never null
   * @param entries for example, {@link org.opencastproject.participation.model.Semester#values()}
   * @return matching per {@link org.opencastproject.participation.model.Semester#getTermCode()}
   */
  public static <T extends RepresentsSalesforceField> T findSalesforceMatch(final String next, final T[] entries) {
    final String name = StringUtils.trimToNull(next);
    for (final T entry : entries) {
      final String value = entry.getSalesforceValue();
      final boolean match = (value == null && name == null) || (value != null && value.equalsIgnoreCase(name));
      if (match) {
        return entry;
      }
    }
    return null;
  }

  static <T extends RepresentsSalesforceField> T findSalesforceMatch(final Map<String, String> properties, final HasSalesforceFieldName fieldName, final T[] values) {
    return findSalesforceMatch(get(properties, fieldName), values);
  }

  public static void setField(final SObject sObject, final HasSalesforceFieldName field, final Object value) {
    sObject.setField(field.name(), value);
  }

  static String get(final Map<String, String> properties, final HasSalesforceFieldName field) {
    return StringUtils.trimToNull(properties.get(field.name()));
  }

  public static String toString(final XmlObject xmlObject) {
    final StringBuilder b = new StringBuilder("[START] toString(XmlObject)\n");
    final Map<String, String> properties = getProperties(xmlObject);
    for (final String key : properties.keySet()) {
      b.append(" [Key]: ").append(key).append('\n')
          .append(" [Value]: ").append(properties.get(key)).append('\n')
          .append(" -----\n");
    }
    b.append("[END] toString(XmlObject)\n");
    return b.toString();
  }

  public static Map<String, String> getProperties(final XmlObject xmlObject) {
    final Iterator<XmlObject> children = xmlObject.getChildren();
    final Map<String, String> properties = new HashMap<String, String>();
    while (children.hasNext()) {
      final XmlObject next = children.next();
      final String localPart = SalesforceUtils.getLocalPart(next);
      if (localPart != null) {
        final String value = (next.getValue() == null) ? null : next.getValue().toString();
        properties.put(localPart, value);
      }
    }
    return properties;
  }

  /**
   * @param instructor with non-null email address
   * @return no change in 'prod' environment. Otherwise, append a suffix to invalidate the email address. We do NOT want to
   * inadvertently email instructors from test environments.
   */
  public static String getSafeEmailAddress(final Instructor instructor) {
    final Environment environment = EnvironmentUtil.getEnvironment();
    return Environment.prod.equals(environment) ? instructor.getEmail() : instructor.getEmail() + "-DISABLE";
  }

  /**
   * @param salesforceFormat for example, 12:50p or 8:00a
   * @return for example, 1250 or 0800
   */
  public static String toMilitaryTime(final String salesforceFormat) {
    final int hoursAndMinutes = new Integer(salesforceFormat.replaceAll("\\D+",""));
    final int militaryTime = (salesforceFormat.contains("p") && hoursAndMinutes < 1200) ? hoursAndMinutes + 1200 : hoursAndMinutes ;
    final String prefix = (militaryTime < 100) ? "00" : (militaryTime < 1000) ? "0" : "";
    return prefix + militaryTime;
  }

  /**
   * @param militaryTime for example, 1250 or 0800
   * @return for example, 12:50p or 8:00a
   */
  public static String toSalesforceTimeFormat(final String militaryTime) {
    final TimeOfDay timeOfDay = DateUtil.getTimeOfDay(militaryTime);
    final boolean isNoon = timeOfDay.getHour() == 0 && TimeOfDay.DayPeriod.PM.equals(timeOfDay.getDayPeriod());
    final String hour = isNoon ? "12" : StringUtil.asTwoDigitString(timeOfDay.getHour());
    final String minutes = StringUtil.asTwoDigitString(timeOfDay.getMinutes());
    return hour + ':' + minutes + (TimeOfDay.DayPeriod.PM == timeOfDay.getDayPeriod() ? 'p' : 'a');
  }

  /**
   * In Salesforce, the listing of instructors for a given course might have positions 1, 3 and 5 taken. This method
   * would return the result of [2, 4, 6] telling you that those positions are available if you intend to add
   * instructors to the course.
   * @param participationSet represents what is currently in Salesforce.
   * @return In Salesforce, for a given course, the positions within list of instructors that are currently null.
   */
  public static SalesforceFieldInstructor[] getAvailableInstructorFields(final Set<Participation> participationSet) {
    int lastNonNullIdentifier = -1;
    for (final Participation p : participationSet) {
      final SalesforceFieldInstructor salesforceField = p.getSalesforceField();
      if (salesforceField != null) {
        final int identifier = p.getSalesforceField().getIdentifier();
        lastNonNullIdentifier = (lastNonNullIdentifier > identifier) ? lastNonNullIdentifier : identifier;
      }
    }
    final Set<SalesforceFieldInstructor> availableFields = new TreeSet<SalesforceFieldInstructor>(new Comparator<SalesforceFieldInstructor>() {
      @Override
      public int compare(final SalesforceFieldInstructor f1, SalesforceFieldInstructor f2) {
        return f1.getIdentifier() - f2.getIdentifier();
      }
    });
    for (final SalesforceFieldInstructor field : SalesforceFieldInstructor.values()) {
      if (field.getIdentifier() > lastNonNullIdentifier) {
        availableFields.add(field);
      }
    }
    if (availableFields.isEmpty()) {
      throw new SalesforceIndexOutOfBoundsException("Salesforce has NO more available SalesforceFieldInstructor positions. Existing participationSet: "
          + ToStringBuilder.reflectionToString(participationSet, ToStringStyle.MULTI_LINE_STYLE));
    }
    return availableFields.toArray(new SalesforceFieldInstructor[availableFields.size()]);
  }

  /**
   * Requirements for saving a set of Participation records to Salesforce:
   * <ul>
   *   <li>The set of records must have six entries.</li>
   *   <li>Each {@link org.opencastproject.participation.model.Participation#getSalesforceField()} must be
   *   unique within the set.</li>
   *   <li>Each {@link org.opencastproject.participation.model.Participation#getSalesforceField()} must be
   *   in the range 1..6.</li>
   * </ul>
   * This method will throw an exception if the conditions above are not met.
   * @param set ready to push to Salesforce.
   * @return map where key reflects {@link org.opencastproject.participation.model.Participation#getSalesforceField()}
   */
  public static SortedMap<SalesforceFieldInstructor, Participation> getValidatedSalesforceIndexMap(final Set<Participation> set) {
    final SortedMap<SalesforceFieldInstructor, Participation> map = new TreeMap<SalesforceFieldInstructor, Participation>();
    for (final Participation p : set) {
      final SalesforceFieldInstructor field = p.getSalesforceField();
      if (field == null) {
        throw new IllegalStateException("Null value for salesforceField: " + p.toString());
      }
      if (map.containsKey(field)) {
        throw new IllegalStateException("Duplicate value for salesforceField: " + p.toString());
      }
      map.put(field, p);
    }
    return map;
  }

  public static ConnectorConfig getConnectorConfig(final Properties properties) {
    final ConnectorConfig connectorConfig = new ConnectorConfig();
    connectorConfig.setUsername(properties.getProperty("salesforce.connector.username"));
    connectorConfig.setPassword(properties.getProperty("salesforce.connector.setPassword"));
    connectorConfig.setAuthEndpoint(properties.getProperty("salesforce.connector.setAuthEndpoint"));
    final Integer timeout = new Integer(properties.getProperty("salesforce.connector.setConnectionTimeout"));
    connectorConfig.setConnectionTimeout(timeout);
    return connectorConfig;
  }

}
