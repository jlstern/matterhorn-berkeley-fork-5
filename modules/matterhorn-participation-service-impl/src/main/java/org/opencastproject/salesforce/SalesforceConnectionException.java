/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

/**
 * @see com.sforce.ws.ConnectionException
 * @author John Crossman
 */
public class SalesforceConnectionException extends RuntimeException {

  /**
   * We require this class to be used as a wrapper on an existing exception.
   * @param s never null
   * @param throwable never null
   */
  public SalesforceConnectionException(final String s, final Throwable throwable) {
    super(s, throwable);
  }
}
