/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;

import com.sforce.soap.partner.sobject.SObject;

import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public interface SalesforceObjectTransformer {

  /**
   * @param record never null
   * @return instance populated with data from records
   */
  CourseOffering getCourseOffering(SObject record);

  /**
   * @param record Null not allowed.
   * @return instance populated with data from records
   */
  Instructor getInstructor(SObject record);

  /**
   * @param record Null not allowed.
   * @return instance populated with data from records
   */
  Room getRoom(SObject record);

  /**
   * @param record never null; Salesforce record
   * @return list with {@link org.opencastproject.participation.model.Participation#getSalesforceField()} set
   */
  Set<Participation> getParticipationSet(SObject record);

  /**
   * update {@link SObject} with data from {@link CourseData} instance
   * @param record never null
   * @param courseData never null
   */
  void setFieldsForCourseUpdate(SObject record, CourseData courseData);

  /**
   * @param courseData never null
   * @return new instance which is ready to pass to {@link SalesforceConnectorService}
   */
  SObject getSObjectForCourseCreation(CourseData courseData);

  /**
   * Convenient method when preparing for upsert on course offering objects.
   * @param records never null; each entry contains course data
   * @return course offering objects mapped to source of data
   */
  Map<CourseOffering, SObject> extractAssociatedCourseOfferings(SObject[] records, Semester semester, Integer year);

  /**
   * @param record never null
   * @return populated with data from record
   */
  Term getTerm(SObject record);

  /**
   * @param record never null
   * @return true when property {@link org.opencastproject.salesforce.entity.CourseField#Recordings_Scheduled__c} is true.
   */
  boolean isRecordingsScheduled(SObject record);

}
