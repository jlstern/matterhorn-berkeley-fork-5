/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author John Crossman
 */
public class DatabaseCredentialsTest {

  @Test
  public void testGetURL() {
    final DatabaseCredentials c = new DatabaseCredentials("dba-oracle-dev-10.ist.berkeley.edu", 1523, "bspdev", "username", "password");
    assertEquals("jdbc:oracle:thin:@dba-oracle-dev-10.ist.berkeley.edu:1523:bspdev", c.getURL());
  }
}
