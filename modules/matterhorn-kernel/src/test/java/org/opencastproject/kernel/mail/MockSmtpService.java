/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.mail;

import org.opencastproject.notify.SendType;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author John Crossman
 */
public class MockSmtpService extends SmtpService {

  private final Session session;
  private MimeMessage lastMessageSent;

  MockSmtpService() {
    session = Session.getInstance(new Properties());
  }

  @Override
  public SendType send(final MimeMessage message) throws MessagingException {
    lastMessageSent = message;
    return super.send(message);
  }

  MimeMessage getLastMessageSent() {
    return lastMessageSent;
  }

  public MimeMessage createMessage() {
    return new MimeMessage(session);
  }

  @Override
  protected Transport getTransport() throws NoSuchProviderException {
    return new Transport(session, new URLName("foo")) {
      @Override
      public void sendMessage(final Message message, final Address[] addresses) throws MessagingException {
        // Do nothing.
      }
      @Override
      public void connect() throws MessagingException {
        // Do nothing.
      }
    };
  }
}