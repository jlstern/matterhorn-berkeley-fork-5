/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.impl;

import static org.junit.Assert.assertEquals;

import org.opencastproject.workflow.api.WorkflowDefinition;
import org.opencastproject.workflow.api.WorkflowDefinitionImpl;

import org.junit.Test;

import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author John Crossman
 */
public class DefaultWorkflowComparatorTest {

  private final String defaultWorkflowID = "foo";

  @Test
  public void testNoMatchWithDefault() {
    final SortedMap<String, WorkflowDefinition> map = getSortedMap("a", "b", "c");
    final String[] keySet = map.keySet().toArray(new String[3]);
    assertEquals("a", keySet[0]);
    assertEquals("b", keySet[1]);
    assertEquals("c", keySet[2]);
  }

  @Test
  public void testMatchFirst() {
    final SortedMap<String, WorkflowDefinition> map = getSortedMap(defaultWorkflowID, "b", "c");
    final String[] keySet = map.keySet().toArray(new String[3]);
    assertEquals(defaultWorkflowID, keySet[0]);
    assertEquals("b", keySet[1]);
    assertEquals("c", keySet[2]);
    //
    assertEquals(defaultWorkflowID, map.get(defaultWorkflowID).getId());
    assertEquals("c", map.get(map.lastKey()).getId());
  }

  @Test
  public void testMatchMiddle() {
    final SortedMap<String, WorkflowDefinition> map = getSortedMap("a", defaultWorkflowID, "c");
    final String[] keySet = map.keySet().toArray(new String[3]);
    assertEquals(defaultWorkflowID, keySet[0]);
    assertEquals("a", keySet[1]);
    assertEquals("c", keySet[2]);
    //
    assertEquals(defaultWorkflowID, map.get(defaultWorkflowID).getId());
    assertEquals("c", map.get(map.lastKey()).getId());
  }

  @Test
  public void testMatchLast() {
    final SortedMap<String, WorkflowDefinition> map = getSortedMap("a", "b", defaultWorkflowID);
    final String[] keySet = map.keySet().toArray(new String[3]);
    assertEquals(defaultWorkflowID, keySet[0]);
    assertEquals("a", keySet[1]);
    assertEquals("b", keySet[2]);
    //
    assertEquals(defaultWorkflowID, map.get(defaultWorkflowID).getId());
    assertEquals("b", map.get(map.lastKey()).getId());
  }

  private SortedMap<String,WorkflowDefinition> getSortedMap(final String... keys) {
    final Comparator<String> comparator = new DefaultWorkflowComparator(defaultWorkflowID);
    final SortedMap<String, WorkflowDefinition> map = new TreeMap<String, WorkflowDefinition>(comparator);
    for (final String key : keys) {
      final WorkflowDefinitionImpl workflowDefinition = new WorkflowDefinitionImpl();
      workflowDefinition.setId(key);
      map.put(key, workflowDefinition);
    }
    return map;
  }

}
